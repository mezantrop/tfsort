// Copyright (c) 2018, Mikhail Zakharov <zmey20000@yahoo.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// tfsort.js - Sort Tumblrs you are following by their last activity
//

// ---------------------------------------------------------------------------------------------------------------------
// Warning! Unstable alpha stage code! Should work with Firefox, other browsers are untested.
// ---------------------------------------------------------------------------------------------------------------------

// Someday it will be a Firefox plugin, maybe. But now you have to do everything yourself:
// 1. Set English in your Tumblr settings: https://www.tumblr.com/settings/account
// 2. Open https://www.tumblr.com/following URL
// 3. Copy the source code into your Firefox Web Console: Menu -> Web Developer -> Web Console.
// 4. Press Enter to run it and wait for results!

//
// v0.1		2018.06.15	zmey20000@yahoo.com	Initial code
// v0.2		2018.06.16	zmey20000@yahoo.com	Minor fixes
//

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

function follow(blog, key) {
	jQuery.post("https://www.tumblr.com/svc/follow",
		{
			form_key: key,
			'data[tumblelog]': blog,
			'data[source]': "FOLLOW_SOURCE_FOLLOWING_PAGE"
		},
		function(data, status) {
			var ufollowbtn = document.getElementById("unfollow_button_" + blog);
			ufollowbtn.style="display: inline;";
			var followbtn = document.getElementById("follow_button_" + blog);
			followbtn.style="display: none;"
		}
	);
}

function unfollow(blog, key) {
	jQuery.post("https://www.tumblr.com/svc/unfollow",
		{
			form_key: key,
			'data[tumblelog]': blog,
			'data[source]': "UNFOLLOW_SOURCE_FOLLOWING_PAGE"
		},
		function(data, status) {
			var ufollowbtn = document.getElementById("unfollow_button_" + blog);
			ufollowbtn.style="display: none;";
			var followbtn = document.getElementById("follow_button_" + blog);
			followbtn.style="display: inline;"
		}
	);
}

function bshow(dir, blogs) {
	var flwr = document.createElement("div");

	// DIV element to process data
	var left_column = document.getElementById("left_column");
	var ftop = document.getElementById("invite_someone");
	if (ftop === null) ftop = "";

	if (!dir) offset = offset - 2 * records - 2 >= 0 ? offset - 2 * records - 2 : 0;
	var limit = offset + records > nFollowing - 1 ? nFollowing - 1 : offset + records;
//	console.log("Limit: ", limit, "Offset: ", offset, "nFollowing: ", nFollowing);
//	console.log(blogs);
	for (offset; offset <= limit; offset++) {
		var cflwr = document.createElement("div");
		cflwr.innerHTML = blogs[offset].braw;

		cflwr.getElementsByClassName("chrome clear big unfollow_button")[0].outerHTML='\
			<button \
				class="chrome clear big unfollow_button" \
				id="unfollow_button_' + blogs[offset].bName + '" \
				data-name="' + blogs[offset].bName + '" \
				data-formkey="' + blogs[offset].bformkey + '" \
				onclick="unfollow(\''+ blogs[offset].bName + '\', \'' +
					blogs[offset].bformkey + '\')">Unfollow</button>';

		cflwr.getElementsByClassName("chrome blue big follow_button")[0].outerHTML='\
			<button \
				class="chrome blue big follow_button" \
				id="follow_button_' + blogs[offset].bName + '" \
				style="display: none;" \
				data-name="' + blogs[offset].bName + '" \
				data-formkey="' + blogs[offset].bformkey + '" \
				onclick="follow(\''+ blogs[offset].bName + '\', \'' +
					blogs[offset].bformkey + '\')">Follow</button>';

        	flwr.innerHTML += cflwr.innerHTML;
	}
	left_column.innerHTML = ftop.outerHTML + flwr.innerHTML + fbottom.outerHTML;
}

function doSort() {
//	console.log('pages.length:', pages.length, 'nFollowing:', nFollowing);
	if (pages.length < nFollowing) return 0;
	clearInterval(myival);

	for (var page in pages) {
		var el = document.createElement("html");
		el.innerHTML = pages[page];

		// Every "el" page contains up to 13 blog entries, but as we have set offset == 1, on each
		// loop iteration we are interested in the first only entry index [0], other blogs are discarded
		// as they are processed lately:
		var fblog = el.getElementsByClassName("follower alt clearfix")[0];

		// Save intersting blog fields into the array of objects
		var blu = fblog.getElementsByClassName("last_updated")[0].innerText.split(' ');
		switch (blu[2].charAt(0) + blu[2].charAt(1)) {
			case "se":
				atime = Number(blu[1]);			// seconds
				break;
			case "mi":
				atime = Number(blu[1]) * 60;		// minutes
				break;
			case "ho":
				atime = Number(blu[1]) * 3600;		// hours: 60 * 60
				break;
			case "da":
				atime = Number(blu[1]) * 86400; 	// days: 60 * 60 * 24
				break;
			case "we":
				atime = Number(blu[1]) * 604800;	// weeks: 60 * 60 * 24 * 7;
				break;
			case "mo":
				atime = Number(blu[1]) * 18144000;	// months; 60 * 60 * 24 * 7 * 30;
				break;
			case "ye":
				atime = Number(blu[1]) * 6622560000;	// years: 60 * 60 * 24 * 7 * 30 * 365;
				break;
			default:
				atime = 662256000000;			// century: 100 * 60 * 60 * 24 * 7 * 30 * 365;
		}

		blogs.push({
			bAvatar: fblog.getElementsByClassName("avatar_img")[0].src,
			bName: fblog.getElementsByClassName("name")[0].innerText,
			bLink: fblog.getElementsByClassName("name-link")[0].href,
			bUpdate_h: blu[1] + " " + blu[2],
			bUpdate_s: atime,
			bformkey: fblog.getElementsByClassName("chrome blue big follow_button")[0].dataset.formkey,
	        	braw: fblog.outerHTML
		});
	}
	console.log("Debug: Sorting");
	blogs.sort(dynamicSort("-bUpdate_s"));
	console.log("Debug: Sorted");
	bshow(1, blogs);
	console.log("Debug: Showed");
}

// -- Main goes here ---------------------------------------------------------------------------------------------------
var offset = 0;					// Offset in the list of followig tumblrs
var records = 24;				// Maximum number of records on the page counted from 0
var blogs = [];					// Here is a list of tumblrs we are following

var fbottom = document.getElementById("pagination");
var nextbtn = document.getElementsByClassName("next button chrome blue")[0];
if (nextbtn)
	nextbtn.parentNode.removeChild(nextbtn);
var prevbtn = document.getElementsByClassName("previous button chrome")[0];
if (prevbtn)
	prevbtn.parentNode.removeChild(prevbtn);

var sortmenextbtn = document.createElement("button");
var sortmeprevbtn = document.createElement("button");
fbottom.appendChild(sortmeprevbtn);
fbottom.appendChild(sortmenextbtn);
sortmeprevbtn.outerHTML="<button class=\"previous big button chrome blue\" onclick=\"bshow(0, blogs)\">PREVIOUS</button>";
sortmenextbtn.outerHTML="<button class=\"next big button chrome blue\" onclick=\"bshow(1, blogs)\">NEXT</button>";

// A number of blogs we are following:
var nFollowing = Number(document.getElementById("tabs").innerText.split(' ')[1].replace(",", ""));

// Here "page" is an offset in a list of blogs we are following. Typically Tumblr shows up to 25 blogs (page += 25)
// in a browser.When we request the same in the script, Tumblr starts working with a smaller offset (page += 10-13)
// blogs per page, and there is some extra НЁХ happens while enumerating them. So lets process them slowly one blog
// by one, i.e. page += 1):
var pages = [];
for (page = 0; page < nFollowing; page++) {
	jQuery.get("https://www.tumblr.com/following/" + String(page), function( following_page ) {
		pages.push(following_page);
		document.getElementById("tabs").getElementsByTagName("A")[0].innerText = "Scanning followed Tumblrs: " +
			pages.length + "/" + nFollowing;
	});
}

var myival = setInterval(doSort, 500);
