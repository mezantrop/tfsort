tfsort.js - Sort Tumblrs you are following by their last activity

---------------------------------------------------------------------------------------------------------
Warning! Unstable alpha stage code! Should work with Firefox, other browsers are untested.
---------------------------------------------------------------------------------------------------------

Someday it will be a Firefox plugin, maybe. But now you have to do everything yourself:
1. Set English in your Tumblr settings: https://www.tumblr.com/settings/account
2. Open https://www.tumblr.com/following URL
3. Copy the source code into your Firefox Web Console: Menu -> Web Developer -> Web Console.
4. Run it and wait for results!

v0.1		2018.06.15	zmey20000@yahoo.com	Initial code
v0.2		2018.06.16	zmey20000@yahoo.com	Minor fixes
